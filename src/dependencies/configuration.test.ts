import {assert} from "chai";
import {newConfiguration} from "./configuration";
import {newScopedLogger} from "@toincrease/node-logger";
import {MemoryConfigurator} from "@toincrease/node-config";
import {Await} from "../util/callback";

describe("Configuration object", () => {
    let name = "thename";
    let l = newScopedLogger();

    let m = new MemoryConfigurator();
    m.set(name, "def");
    m.set(name + "/abc", "123");
    m.set("local", true);
    m.set("region", "eu-west-1");
    m.set(name + `/EnvironmentStackName`, "xyz");

    let w = new Await(this.logger, () => {
        return;
    });
    let c = newConfiguration(name, l, w, m);
    w.wrapperComplete();

    it("should have convenient properties.", () => {
        assert.isTrue(c.local.value());
    });
    it("should be able to get a key.", (done) => {
        c.getString(name, "___").subscribe(
            (v) => assert.equal(v.value(), "def"),
            (e) => done(e),
            () => done()
        );
    });
    it("should be able to get a prefixed key with a prefixed slash.", (done) => {
        c.getPrefixedString("/abc", "___").subscribe(
            (v) => assert.equal(v.value(), "123"),
            (e) => done(e),
            () => done()
        );
    });
    it("should be able to get a prefixed key.", (done) => {
        c.getPrefixedString("abc", "___").subscribe(
            (v) => assert.equal(v.value(), "123"),
            (e) => done(e),
            () => done()
        );
    });
    it("should be able to get the stack name", () => {
        assert.equal(c.getExternalBaseUri(), "https://xyz.innius.com");
    });
});
