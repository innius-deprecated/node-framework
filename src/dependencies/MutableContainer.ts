import {IContainerBuilder, IContainer, createBuilder} from "typeioc";
import {newScopedLogger} from "@toincrease/node-logger";
import {Container} from "./Container";
import {Configuration, newConfiguration} from "./configuration";
import {Await} from "../util/callback";

export class MutableContainer implements Container {
    private rwcontainer: IContainerBuilder;
    private container: IContainer;
    private config: Configuration;

    constructor(servicename: string = "") {
        this.rwcontainer = createBuilder();
        let l = newScopedLogger();
        let w = new Await(l, () => {
            return;
        });
        this.config = newConfiguration(servicename, l, w);
    }

    setConfiguration(c: Configuration): void {
        this.config = c;
    }

    configuration(): Configuration {
        return this.config;
    }

    asContainer(): Container {
        this.container = this.rwcontainer.build();
        return this;
    }

    register(service: any, val: any): void {
        this.rwcontainer.register(service).as(() => val);
    }

    registerThunk(service: any, thunk: () => any): void {
        this.rwcontainer.register(service).as(thunk);
    }

    resolve<T>(service: any): T {
        return this.container.resolve<T>(service);
    }
}
