import {assert} from "chai";
import {MutableContainer} from "./MutableContainer";

class Foo {
    constructor(public value: string) { }
}

describe("MutableContainer & Container", () => {
    describe("#State transition", () => {
        it("should be able to switch to a Container.", () => {
            const s = new MutableContainer();
            const c = s.asContainer();
            assert.notEqual(c, undefined);
        });
        it("should be able to store and retrieve a reference.", () => {
            const s = new MutableContainer();

            for (var i = 0; i < 1; i++) {
                const f = new Foo("hello, world");
                assert.equal("hello, world", f.value);
                s.register(Foo, f);
            }

            const c = s.asContainer();

            const g = c.resolve<Foo>(Foo);

            assert.equal("hello, world", g.value);
        });
        it("should be able to store and retrieve a thunk.", () => {
            const s = new MutableContainer();

            for (var i = 0; i < 1; i++) {
                s.registerThunk(Foo, () => new Foo("hello, world"));
            }

            const c = s.asContainer();

            const g = c.resolve<Foo>(Foo);

            assert.equal("hello, world", g.value);
        });
    });
});
