import {Configuration as C, newConfiguration as nc, Configurator, DynamicValue, Change} from "@toincrease/node-config";
import {Observable} from "@reactivex/rxjs";
import { Logger } from "@toincrease/node-logger";
import {NetworkHttp} from "@toincrease/node-http";
import {AwaitCallback} from "../util/callback";

/**
 * A decorator for the node-config Configurator
 */
export interface Configuration extends C {
    region: DynamicValue<string>;
    local: DynamicValue<boolean>;
    loglevel: DynamicValue<string>;
    /**
     * The authentication service endpoint     
     */
    authentication_endpoint: DynamicValue<string>;
    /**
     * The authorization service endpoint     
    */
    authorization_endpoint: DynamicValue<string>;
    /**
     * The dynamodb endpoint.
     */
    dynamodb_endpoint: DynamicValue<string>;
    /**
     * Is there a dynamodb endpoint configured?
     * @returns boolean An indication on whether an endpoint is configured. If it does not exist, the default is assumed.
     */
    hasDynamoDBEndpoint(): boolean;
    /**
     * Retrieve a dynamic value representing a number. 
     * Do not access a raw key, but use the currently configured service name as prefix.
     * @param key The key of this dynamic value.
     * @param def The default initial value of this dynamic value.
     */
    getPrefixedNumber(key: string, def: number): Observable<DynamicValue<number>>;
    /**
     * Retrieve a dynamic value representing a string.
     * Do not access a raw key, but use the currently configured service name as prefix.
     * @param key The key of this dynamic value.
     * @param def The default initial value of this dynamic value.
     */
    getPrefixedString(key: string, def: string): Observable<DynamicValue<string>>;
    /**
     * Retrieve a dynamic value representing a bool.
     * Do not access a raw key, but use the currently configured service name as prefix.
     * @param key The key of this dynamic value.
     * @param def The default initial value of this dynamic value.
     */
    getPrefixedBool(key: string, def: boolean): Observable<DynamicValue<boolean>>;

    /**
     * Return the external base uri for this service.
     * @example https://xenon.innius.com/api/foo-service
     */
    getExternalServiceBaseUri(): string;
    /**
     * Return the external base uri for this stack.
     * @example https://xenon.innius.com
     */
    getExternalBaseUri(): string;

    /**
     * Retrieve my ip address
     */
    getMyPrivateIpv4(): Observable<string>;
}

export function newConfiguration(servicename: string, log: Logger, waiter: AwaitCallback, ...configurators: Configurator[]): Configuration {
    return new SDKConfiguration(servicename, nc(log, ...configurators), log, waiter);
}

interface ConfValues {
    stack: DynamicValue<string>;
    region: DynamicValue<string>;
    local: DynamicValue<boolean>;
    loglevel: DynamicValue<string>;
    authentication_endpoint: DynamicValue<string>;
    authorization_endpoint: DynamicValue<string>;
    dynamodb_endpoint: DynamicValue<string>;
}

class SDKConfiguration implements Configuration {
    private _configuration: C;
    private name: string;
    private stack: DynamicValue<string>;

    region: DynamicValue<string>;
    local: DynamicValue<boolean>;
    loglevel: DynamicValue<string>;
    authentication_endpoint: DynamicValue<string>;
    authorization_endpoint: DynamicValue<string>;
    dynamodb_endpoint: DynamicValue<string>;

    constructor(name: string, c: C, logger: Logger, waiter: AwaitCallback) {
        this.name = name;
        this._configuration = c;

        const that = this;

        waiter.expect(1);

        Observable.zip(
            c.getBool("/local", false),
            c.getMandatoryString(`/${name}/EnvironmentStackName`),
            c.getMandatoryString("/region"),
            c.getString(`/${name}/loglevel`, "info"),
            c.getString(`/${name}/authentication_endpoint`, ""),
            c.getString(`/${name}/authorization_endpoint`, ""),
            c.getString(`/${name}/dynamodb_endpoint`, ""),
            (a, b, c, d, e, f, g) => {
                return <ConfValues>{
                    stack: b,
                    region: c,
                    local: a,
                    loglevel: d,
                    authentication_endpoint: e,
                    authorization_endpoint: f,
                    dynamodb_endpoint: g
                };
            }
        ).subscribe(
            (values: ConfValues) => {
                that.stack = values.stack;
                that.region = values.region;
                that.local = values.local;
                that.loglevel = values.loglevel;
                that.authentication_endpoint = values.authentication_endpoint;
                that.authorization_endpoint = values.authorization_endpoint;
                that.dynamodb_endpoint = values.dynamodb_endpoint;
            }, (e) => {
                logger.error("Initialization of SDK configuration failed: ");
                logger.error(e);
                waiter.ack();
            }, () => {
                waiter.ack();
            }
        );
    }

    private baseuri_stack(): string {
        return `https://${this.stack.value()}.innius.com`;
    }

    private baseuri_svc(): string {
        return `${this.baseuri_stack()}/api/${this.name}`;
    }

    hasDynamoDBEndpoint(): boolean {
        return this.dynamodb_endpoint.value() !== "";
    }

    dispose(): void {
        this._configuration.dispose();
    }

    getNumber(key: string, def: number): Observable<DynamicValue<number>> {
        return this._configuration.getNumber(key, def);
    }

    getString(key: string, def: string): Observable<DynamicValue<string>> {
        return this._configuration.getString(key, def);
    }

    getBool(key: string, def: boolean): Observable<DynamicValue<boolean>> {
        return this._configuration.getBool(key, def);
    }

    getPrefixedNumber(key: string, def: number): Observable<DynamicValue<number>> {
        return this.getNumber(`/${this.name}/${key}`, def);
    }

    getPrefixedString(key: string, def: string): Observable<DynamicValue<string>> {
        return this.getString(`/${this.name}/${key}`, def);
    }

    getPrefixedBool(key: string, def: boolean): Observable<DynamicValue<boolean>> {
        return this.getBool(`/${this.name}/${key}`, def);
    }

    getMandatoryNumber(key: string): Observable<DynamicValue<number>> {
        return this._configuration.getMandatoryNumber(key);
    }

    getMandatoryString(key: string): Observable<DynamicValue<string>> {
        return this._configuration.getMandatoryString(key);
    }

    getMandatoryBool(key: string): Observable<DynamicValue<boolean>> {
        return this._configuration.getMandatoryBool(key);
    }

    getExternalServiceBaseUri(): string {
        return this.baseuri_svc();
    }

    getExternalBaseUri(): string {
        return this.baseuri_stack();
    }

    getMyPrivateIpv4():Observable<string> {
        let http = new NetworkHttp();
        return http
            .get("http://169.254.169.254/latest/meta-data/local-ipv4")
            .timeout(500)
            .map((x) => <string>x);
    }

    changes(): Observable<Change> {
        return this._configuration.changes();
    }
}
