import {Configuration} from "./configuration";

export interface Container {
    /**
     * Resolve a dependency from within this container. It has to be registered in advance.
     *
     * Resolving something can be done by providing a key and a type parameter. For example, say we have an
     * interface IFoo and a class Foo that implements it, we can resolve it like this:
     * @example c.resolve<IFoo>(Foo);
     * @param service The service you want to look for.
     * @returns T The typed service.
     */
    resolve<T>(service: any): T;
    /**
     * Retrieve the configuration object of this microservice.
     * @returns Configuration the configuration object.
     */
    configuration(): Configuration;
}
