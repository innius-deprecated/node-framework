import {Handler, handle} from "../router/handler";
import {MutableContainer} from "../dependencies/MutableContainer";
import {TestResponseContext} from "@toincrease/node-context";
import {TestRequestContextBuilder, TestRequestContext, newTestRequestContextBuilder} from "@toincrease/node-context";
import {Route} from "../router/Route";
import {jsonSink} from "../subscriber/json";
import {Sink} from "../subscriber/signature";

export class HandlerTestWrapper {
    private ctx: TestRequestContext;

    handler: Handler;
    next: (err?: any) => void;
    context: TestRequestContextBuilder;
    container: MutableContainer;
    sink: Sink;

    /**
     * Provide the TestRequestContext of this HandlerTestWrapper.
     * @returns {TestRequestContext}
     */
    testRequest(): TestRequestContext {
        return this.ctx;
    }

    /**
     * Provide the TestResponseContext of this HandlerTestWrapper.
     * @returns {TestResponseContext}
     */
    testResponse(): TestResponseContext {
        return this.ctx.testResponse();
    }

    /**
     * Convenience method to get the response directly.
     * @returns {any} The recorded respnose.
     */
    getResponse(): any {
        return this.testResponse().getResponse();
    }

    /**
     * Convenience method to get the response status code directly.
     * @returns {number} The status code.
     */
    getStatus(): any {
        return this.testResponse().getStatus();
    }

    /**
     * Execute this handler test with the current HandlerTestWrapper configuration.
     * The build() method of the TestRequestContextBuilder will automatically be called.
     */
    execute(): void {
        this.ctx = this.context.build();
        let r = <Route>{
            handler: this.handler,
            sink: this.sink,
        };
        handle(r, this.ctx, this.container.asContainer());
    }
}

export function testHandler(h: Handler): HandlerTestWrapper {
    let wrapper = new HandlerTestWrapper();

    wrapper.handler = h;
    wrapper.next = () => "";
    wrapper.context = newTestRequestContextBuilder((err?: any) => wrapper.next(err));
    wrapper.container = new MutableContainer("abc");
    wrapper.sink = jsonSink;

    return wrapper;
}
