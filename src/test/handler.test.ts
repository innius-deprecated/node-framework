import {assert} from "chai";
import {testHandler} from "./handler";
import {RequestContext} from "@toincrease/node-context";
import {Container} from "../dependencies/Container";
import {Observable} from "@reactivex/rxjs";

describe("test#handler", () => {
    it("should be able to test void handlers easily", () => {
        let f = (c: RequestContext, d: Container): void => {
            c.response().send(c.params("abc"), 201);
        };
        let h = testHandler(f);

        h.context.withParam("abc", "def");
        h.execute();

        assert.equal(h.getResponse(), "def");
        assert.equal(h.getStatus(), 201);
    });
    it("should be able to test observable handlers easily", () => {
        let f = (c: RequestContext, d: Container): Observable<any> => {
            return Observable.of({ abc: false });
        };
        let h = testHandler(f);

        h.execute();

        assert.deepEqual(h.getResponse(), { abc: false });
        assert.equal(h.getStatus(), 200);
    });
    it("should be able to test async by setting a next func", (done) => {
        let f = (c: RequestContext, d: Container): Observable<any> => {
            return Observable.of({ abc: false });
        };
        let h = testHandler(f);
        h.next = done;
        h.execute();
    });
    it("should be able to test async by fluently providing a callback.", (done) => {
        let f = (c: RequestContext, d: Container): Observable<any> => {
            return Observable.of(c.params("a"));
        };
        let h = testHandler(f);
        h.context.withParam("a", "b").withCallback(done);
        h.execute();
    });
    it("should be able to cope with uncaught exceptions", (done) => {
        let f = (c: RequestContext, d: Container): void => {
            throw new Error("Oopsie");
        };
        let h = testHandler(f);
        h.context.withNext((err) => {
            assert.deepEqual(h.getResponse(), {
                code: "InternalServerError",
                message: "Oopsie"
            });
            assert.equal(h.getStatus(), 500);
            done(err);
        });
        h.execute();
    });
    it("should be able to reproduce test ron.", (done) => {
        let f = (c: RequestContext, d: Container) => {
            Observable.empty().subscribe(
                null,
                (err) => c.response().error(err),
                () => c.response().send("abc")
            );
            return;
        };
        let h = testHandler(f);
        h.context
            .withParam("a", "b")
            .withCallback((err) => {
                assert.equal(h.getResponse(), "abc");
                assert.equal(h.getStatus(), 200);
                done(err);
            });
        h.execute();
    });
});
