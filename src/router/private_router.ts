import {Container} from "../dependencies/Container";
import {Route} from "./Route";

import {Server, Request, Response, Next} from "restify";
import {ProtectedRouterImpl} from "./protected_router";

export class PrivateRouterImpl extends ProtectedRouterImpl {
    /**
     * Apply the routes of this router to a given server.
     * @param service The name of the service
     * @param server The server to register the routes at
     * @param c A dependencies container.
     */
    apply(service: string, server: Server, c: Container): void {
        let that = this;
        this.routes.forEach((r: Route) => {
            let restify_handler = that.registerRoute(service, r, c);
            let private_guard =
                (h: (req: Request, res: Response, next: Next) => any): (req: Request, res: Response, next: Next) => any => {
                    return (req: Request, res: Response, next: Next): any => {
                        let header = req.header("X-Forwarded-For");
                        if (header) {
                            res.send(403, "Internal usage only.");
                        } else {
                            h(req, res, next);
                        }
                    };
                };
            server[r.method](r.uri, private_guard(restify_handler));
            // register the same route again under /service-name to use enable usage of the service without rewriting urls
            server[r.method]("/".concat(service, r.uri), private_guard(restify_handler));
        });
    }
}
