import {assert} from "chai";
import {Router} from "./Router";
import {ProtectedRouterImpl} from "./protected_router";
import {Container} from "../dependencies/Container";
import {Observable} from "@reactivex/rxjs";
import {MutableContainer} from "../dependencies/MutableContainer";
import {RequestContext, TestRequestContext, newTestRequestContextBuilder} from "@toincrease/node-context";
import {Server} from "restify";
import {Handler} from "../router/handler";
import {jsonSink} from "../subscriber/json";
import {newScopedLogger} from "@toincrease/node-logger";
import {MemoryConfigurator} from "@toincrease/node-config";
import {Await} from "../util/callback";
import {newConfiguration} from "../dependencies/configuration";

function hook(method: string, srv: any): (uri: string, handler: Handler) => void {
    return (uri: string, handler: Handler): void => {
        srv[uri + "::" + method] = handler;
    };
}

var test_handle = (server: any, route: string, ctx: TestRequestContext): void =>
    server[route](ctx, ctx.testResponse().raw_response(), ctx.testResponse().nextFunc());

describe("Router", () => {
    var simple;
    var rx;
    let name = "name";

    var simpleHandler = (c: RequestContext, d: Container): void => {
        simple++;
        return;
    };
    var rxHandler = (c: RequestContext, d: Container): Observable<any> => {
        rx++;
        return Observable.of({ abc: true });
    };

    var rwcontainer = new MutableContainer();
    var container = rwcontainer.asContainer();

    let l = newScopedLogger();
    let m = new MemoryConfigurator();
    m.set("local", true);
    m.set("region", "eu-west-1");
    m.set(`${name}/EnvironmentStackName`, "xyz");

    let w = new Await(this.logger, () => {
        return;
    });
    let c = newConfiguration(name, l, w, m);
    w.wrapperComplete();
    rwcontainer.setConfiguration(c);

    var server_invoke = {};

    var server = <Server>{
        "get": hook("get", server_invoke),
        "put": hook("put", server_invoke),
        "del": hook("del", server_invoke),
        "post": hook("post", server_invoke),
        "head": hook("head", server_invoke),
    };

    it("should be able to handle a simple response.", () => {
        simple = 0;
        var sr = new ProtectedRouterImpl();
        var r = new Router(sr);
        var ctx = newTestRequestContextBuilder().build();

        r.get("/", (c: RequestContext, d: Container): void => {
            c.response().json("body", 201);
        });
        r.apply(name, server, container);

        test_handle(server_invoke, "/::get", ctx);

        assert.equal("body", ctx.testResponse().getResponse());
        assert.equal(201, ctx.testResponse().getStatus());
    });
    it("should be able to register a simple route and call it.", () => {
        simple = 0;
        var r = new Router(new ProtectedRouterImpl());
        var ctx = newTestRequestContextBuilder().build();

        r.get("/", simpleHandler);
        r.apply(name, server, container);
        test_handle(server_invoke, "/::get", ctx);

        assert.equal(1, simple);
    });
    it("should be able to register a route for a sink and call it.", () => {
        rx = 0;
        var r = new Router(new ProtectedRouterImpl());
        var ctx = newTestRequestContextBuilder().build();

        r.del("/rx", rxHandler);
        r.apply(name, server, container);

        test_handle(server_invoke, "/rx::del", ctx);

        assert.equal(1, rx);
        assert.deepEqual({ abc: true }, ctx.testResponse().getResponse());
        assert.equal(200, ctx.testResponse().getStatus());
    });
    it("should be able to handle errors.", () => {
        simple = 0;
        var r = new Router(new ProtectedRouterImpl());
        var ctx = newTestRequestContextBuilder().build();

        r.get("/", (c: RequestContext, d: Container): void => {
            c.response().error(new Error("foo"));
        });
        r.apply("myname", server, container);

        test_handle(server_invoke, "/::get", ctx);

        assert.deepEqual({ code: "InternalServerError", message: "foo" }, ctx.testResponse().getResponse());
        assert.equal(500, ctx.testResponse().getStatus());
    });
    it("should be able to handle uncaught errors.", () => {
        simple = 0;
        var r = new Router(new ProtectedRouterImpl());
        var ctx = newTestRequestContextBuilder().build();

        r.get("/", (c: RequestContext, d: Container): void => {
            throw new Error("bar");
        });
        r.apply(name, server, container);

        test_handle(server_invoke, "/::get", ctx);

        assert.deepEqual({ code: "InternalServerError", message: "bar" }, ctx.testResponse().getResponse());
        assert.equal(500, ctx.testResponse().getStatus());
    });
    it("should be able to register a route and a sink and call it.", (done) => {
        rx = 0;
        var r = new Router(new ProtectedRouterImpl());
        var ctx = newTestRequestContextBuilder().build();

        r.del("/rx", rxHandler, jsonSink);
        r.apply(name, server, container);

        test_handle(server_invoke, "/rx::del", ctx);

        setTimeout(() => {
            assert.deepEqual({ abc: true }, ctx.testResponse().getResponse());
            assert.equal(1, rx);
            done();
        }, 10);
    });
});
