import {RequestContext} from "@toincrease/node-context";
import {Container} from "../dependencies/Container";
import {Observable} from "@reactivex/rxjs";
import {Route} from "./Route";

export interface Handler extends Function {
    (c: RequestContext, di: Container): Observable<any> | void;
}

/**
 * Handle a route func.
 * @param r The route
 * @param rctx Request Context
 * @param c Dependency container.
 */
export function handle(r: Route, rctx: RequestContext, c: Container): void {
    try {
        var ret = r.handler(rctx, c);
        if (typeof ret === "undefined") {
            // void -- return;
            return;
        } else if (ret instanceof Observable) {
            if (r.sink) {
                let obs = <Observable<any>>ret;
                r.sink(obs, rctx);
            }
        }
    } catch (e) {
        rctx.response().error(e);
    }
}
