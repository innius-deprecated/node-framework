import {Sink} from "../subscriber/signature";
import {Handler} from "./handler";

import {Action} from "../proxy/auth";
import {Provider} from "./protected_router";

export class Route {
    constructor(
        public method: string,
        public uri: string,
        public handler: Handler,
        public sink: Sink,
        public action: Action,
        public provider: Provider
    ) { }
}
