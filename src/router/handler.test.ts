import {assert} from "chai";
import {handle} from "./handler";
import {RequestContext, newTestRequestContextBuilder} from "@toincrease/node-context";
import {Container} from "../dependencies/Container";
import {MutableContainer} from "../dependencies/MutableContainer";
import {Observable} from "@reactivex/rxjs";
import {Route} from "./Route";
import {jsonSink} from "../subscriber/json";
import {jsonEmpty404Sink} from "../subscriber/jsonEmpty404";

describe("Handle", () => {
    it("should be able to return manually.", () => {
        var called = false;

        var rctx = newTestRequestContextBuilder().build();
        var container = new MutableContainer();

        let f = (c: RequestContext, d: Container): void => {
            called = true;
            c.response().send("abc");
        };
        let r = <Route>{
            handler: f,
        };

        handle(r, rctx, container.asContainer());

        assert.equal(called, true);
        assert.equal(rctx.testResponse().getResponse(), "abc");
    });
    it("should be able to return via an observable.", () => {
        var called = false;

        var rctx = newTestRequestContextBuilder(() => {
            called = true;
        }).build();
        var container = new MutableContainer();

        let f = (c: RequestContext, d: Container): Observable<string> => {
            return Observable.of("abc");
        };
        let r = <Route>{
            handler: f,
            sink: jsonSink,
        };

        handle(r, rctx, container.asContainer());

        assert.equal(called, true);
        assert.equal(rctx.testResponse().getResponse(), "abc");
    });
    it("should be able to return via another observable.", () => {
        var called = false;

        var rctx = newTestRequestContextBuilder(() => {
            called = true;
        }).build();
        var container = new MutableContainer();

        let f = (c: RequestContext, d: Container): Observable<string> => {
            return Observable.empty();
        };
        let r = <Route>{
            handler: f,
            sink: jsonEmpty404Sink,
        };

        handle(r, rctx, container.asContainer());

        assert.equal(called, true);
        assert.deepEqual(rctx.testResponse().getResponse(), {
            code: "NotFoundError",
            message: "Requested item not found"
        });
        assert.equal(rctx.testResponse().getStatus(), 404);
    });
});
