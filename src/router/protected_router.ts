import {Handler, handle} from "./handler";
import {Container} from "../dependencies/Container";
import {Sink} from "../subscriber/signature";
import {jsonSink} from "../subscriber/json";
import {Route} from "./Route";
import {newRequestData, RequestData} from "./request_data";
import {Action} from "../proxy/auth";
import {TRN} from "../proxy/trn";
import {metrics} from "@toincrease/node-metrics";

import {Server, Request, Response, Next} from "restify";
import * as uuid from "node-uuid";
import {hostname} from "os";

import {newRequestContext, MiddlewareRequestContext, MiddlewareRegistratorContext} from "@toincrease/node-context";
import {AuthenticationHandler, AuthorizationHandler} from "@toincrease/node-auth-middleware";
import {authorization, authentication} from "@toincrease/node-sdk";
import {newScopedLogger} from "@toincrease/node-logger";
import {uri_as_metrics_key} from "./metrics_transformations";

export interface Provider {
    (r: RequestData): TRN;
}

// TODO: Define 'action' and 'provider' types.
export interface ProtectedRouter {
    get(uri: string, handler: Handler, action: Action, provider: Provider, sink?: Sink): void;
    put(uri: string, handler: Handler, action: Action, provider: Provider, sink?: Sink): void;
    post(uri: string, handler: Handler, action: Action, provider: Provider, sink?: Sink): void;
    del(uri: string, handler: Handler, action: Action, provider: Provider, sink?: Sink): void;
    options(uri: string, handler: Handler, action: Action, provider: Provider, sink?: Sink): void;
    head(uri: string, handler: Handler, action: Action, provider: Provider, sink?: Sink): void;
    patch(uri: string, handler: Handler, action: Action, provider: Provider, sink?: Sink): void;
}

export class ProtectedRouterImpl implements ProtectedRouter {
    protected routes: Route[] = [];

    /**
     * Register a 'get' endpoint.
     * @param uri The URI of the endpoint.
     * @param handler A handler func to invoke.
     * @param action An action to secure with
     * @param provider A provider func for a TRN.
     * @param sink An optional response sink when returning an observable
     */
    get(uri: string, handler: Handler, action: Action, provider: Provider, sink?: Sink): void {
        this.addRoute("get", uri, handler, action, provider, sink);
    }
    /**
     * Register a 'put' endpoint.
     * @param uri The URI of the endpoint.
     * @param handler A handler func to invoke.
     * @param action An action to secure with
     * @param provider A provider func for a TRN.
     * @param sink An optional response sink when returning an observable
     */
    put(uri: string, handler: Handler, action: Action, provider: Provider, sink?: Sink): void {
        this.addRoute("put", uri, handler, action, provider, sink);
    }
    /**
     * Register a 'post' endpoint.
     * @param uri The URI of the endpoint.
     * @param handler A handler func to invoke.
     * @param action An action to secure with
     * @param provider A provider func for a TRN.
     * @param sink An optional response sink when returning an observable
     */
    post(uri: string, handler: Handler, action: Action, provider: Provider, sink?: Sink): void {
        this.addRoute("post", uri, handler, action, provider, sink);
    }
    /**
     * Register a 'del' (or delete) endpoint.
     * @param uri The URI of the endpoint.
     * @param handler A handler func to invoke.
     * @param action An action to secure with
     * @param provider A provider func for a TRN.
     * @param sink An optional response sink when returning an observable
     */
    del(uri: string, handler: Handler, action: Action, provider: Provider, sink?: Sink): void {
        this.addRoute("del", uri, handler, action, provider, sink);
    }
    /**
     * Register an 'options' endpoint.
     * @param uri The URI of the endpoint.
     * @param handler A handler func to invoke.
     * @param action An action to secure with
     * @param provider A provider func for a TRN.
     * @param sink An optional response sink when returning an observable
     */
    options(uri: string, handler: Handler, action: Action, provider: Provider, sink?: Sink): void {
        this.addRoute("options", uri, handler, action, provider, sink);
    }
    /**
     * Register a 'head' endpoint.
     * @param uri The URI of the endpoint.
     * @param handler A handler func to invoke.
     * @param action An action to secure with
     * @param provider A provider func for a TRN.
     * @param sink An optional response sink when returning an observable
     */
    head(uri: string, handler: Handler, action: Action, provider: Provider, sink?: Sink): void {
        this.addRoute("head", uri, handler, action, provider, sink);
    }
    /**
     * Register a 'patch' endpoint.
     * @param uri The URI of the endpoint.
     * @param handler A handler func to invoke.
     * @param action An action to secure with
     * @param provider A provider func for a TRN.
     * @param sink An optional response sink when returning an observable
     */
    patch(uri: string, handler: Handler, action: Action, provider: Provider, sink?: Sink): void {
        this.addRoute("patch", uri, handler, action, provider, sink);
    }

    /**
     * Apply the routes of this router to a given server.
     * @param service The name of the service
     * @param server The server to register the routes at
     * @param c A dependencies container.
     */
    apply(service: string, server: Server, c: Container): void {
        let that = this;
        this.routes.forEach((r: Route) => {
            server[r.method](r.uri, that.registerRoute(service, r, c));            
            server[r.method]("/".concat(service, r.uri), that.registerRoute(service, r, c));

        });
    }

    protected registerRoute(service: string, r: Route, c: Container): (req: Request, res: Response, next: Next) => any {
        const method = r.method;
        const _hostname = hostname();
        const lvl = c.configuration().loglevel;

        return (req: Request, res: Response, next: Next): any => {
            let fields = {
                name: service,
                hostname: _hostname,
                request: req.href(),
                method: method,
                request_id: undefined,
                "x-forwarded-host": undefined,
                "x-forwarded-for": undefined,
                "x-real-ip": undefined
            };

            // The default level is the current loglevel of the whole microservice.
            let level = lvl.value();

            for (var header in req.headers) {
                if (req.headers.hasOwnProperty(header)) {
                    let lh = header.toLowerCase();
                    let val = req.headers[header];
                    switch (lh) {
                        case "x-request-id":
                            fields["request_id"] = val;
                            break;
                        case "x-forwarded-host":
                            fields["x-forwarded-host"] = val;
                            break;
                        case "x-forwarded-for":
                            fields["x-forwarded-for"] = val;
                            break;
                        case "x-real-ip":
                            fields["x-real-ip"] = val;
                            break;
                        case "x-log-level":
                            level = val;
                            break;
                    }
                }
            }

            if (!fields.request_id) {
                fields.request_id = uuid.v4();
            }

            var l = newScopedLogger(fields);
            l.setLevel(level);

            // These casts are all allowed since the various contexts are but different perspectives of the
            // originating request context.
            let rctx = newRequestContext(req, res, next, l);
            let f: MiddlewareRequestContext = <MiddlewareRequestContext>rctx;
            let g: MiddlewareRegistratorContext = <MiddlewareRegistratorContext>rctx;

            // The handler is a special case of middleware, will be registered first (and executed last)
            g.registerMiddleware((ctx: MiddlewareRequestContext) => {
                metrics.increment(`handlers.${uri_as_metrics_key(r.method, r.uri)}.count`);
                handle(r, rctx, c);
            });

            // Optionally register auth and other middleware.
            if (r.provider) {
                metrics.increment(`handlers.${uri_as_metrics_key(r.method, r.uri)}.mwcount`);
                // Auth requires a trn (which can throw and only execute after the auth step.)
                const authorization_api = c.resolve<authorization.AuthorizationClient>(authorization.AuthorizationClient);
                g.registerMiddleware((c: MiddlewareRequestContext) => {
                    const trn = r.provider(newRequestData(rctx));
                    AuthorizationHandler(authorization_api, trn, r.action)(c);
                });

                // Execute auth first, register last.
                const authentication_api = c.resolve<authentication.AuthenticationClient>(authentication.AuthenticationClient);
                g.registerMiddleware(AuthenticationHandler(authentication_api));
            }

            // Start with the first middleware handler, working up until the handler is executed.
            f.next();
        };
    }

    private addRoute(method: string, uri: string, handler: Handler, action: Action, provider: Provider, sink?: Sink): void {
        if (!sink) {
            sink = jsonSink;
        }
        this.routes.push(new Route(
            method,
            uri,
            handler,
            sink,
            action,
            provider
        ));
    }
}
