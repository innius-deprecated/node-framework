import {assert} from "chai";
import {uri_as_metrics_key} from "./metrics_transformations";

describe("transform", () => {
    let uris = [
        {
            method: "get",
            uri: "/foo/bar",
            expected: "foo.bar.get"
        },
        {
            method: "post",
            uri: "/foo/:name",
            expected: "foo.name.post"
        },
        {
            method: "put",
            uri: "/:name/other",
            expected: "name.other.put"
        }
    ];
    it("should be able to transform uris", () => {
        uris.forEach((sut) => {
            assert.equal(uri_as_metrics_key(sut.method, sut.uri), sut.expected);
        });
    });
});
