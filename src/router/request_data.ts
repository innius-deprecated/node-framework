import {RequestContext} from "@toincrease/node-context";

export interface RequestData {
    /**
     * Return the company associated with the issuer of the request.
     * @returns string The company.
     */
    company(): string;
    /**
     * Return the user that issued the request.
     * @returns string The user.
     */
    user(): string;
    /**
     * Check whether there is a user associated with this request.
     * @returns boolean Predicate.
     */
    hasUser(): boolean;
    /**
     * Retrieve a parameter from the request, if it exists.
     * @param p The parameter name.
     * @returns string The parameter value.
     */
    param(p: string): string;
    /**
     * @returns string The path of the request.
     */
    path(): string;
}

class Rd implements RequestData {
    constructor(private ctx: RequestContext) { }

    company(): string {
        return this.ctx.claim().userMetadata.companyId;
    }

    user(): string {
        return this.ctx.claim().userMetadata.userId;
    }

    hasUser(): boolean {
        return this.ctx.claim().hasUserId();
    }

    param(p: string): string {
        return this.ctx.params(p);
    }

    path(): string {
        return this.ctx.path();
    }
}

export function newRequestData(ctx: RequestContext): RequestData {
    return new Rd(ctx);
}
