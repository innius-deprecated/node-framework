/**
 * Transform a uri::method tuple to a partial bucket for metric collection.
 * @example /foo/bar :: GET becomes foo.bar.get
 * @example /foo/bar :: POST becomes foo.bar.post
 *
 * @param method
 * @param uri
 * @returns {string}
 */
export function uri_as_metrics_key(method: string, uri: string): string {
    let dashed = uri.replace(/\//g, ".");
    var finalized = `${dashed.replace(/:/g, "")}.${method}`;
    if(finalized.charAt(0) === ".") {
        finalized = finalized.substring(1);
    }
    return finalized;
}
