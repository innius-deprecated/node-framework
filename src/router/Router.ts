import {Handler} from "./handler";

import {ProtectedRouterImpl} from "./protected_router";
import {Sink} from "../subscriber/signature";

import {Server} from "restify";
import {Container} from "../dependencies/Container";

export class Router {
    constructor(private router: ProtectedRouterImpl) { }
    /**
     * Register a 'get' endpoint.
     * @param uri The URI of the endpoint.
     * @param handler A handler func to invoke.
     * @param sink An optional response sink when returning an observable
     */
    get(uri: string, handler: Handler, sink?: Sink): void {
        this.router.get(uri, handler, undefined, undefined, sink);
    }
    /**
     * Register a 'put' endpoint.
     * @param uri The URI of the endpoint.
     * @param handler A handler func to invoke.
     * @param sink An optional response sink when returning an observable
     */
    put(uri: string, handler: Handler, sink?: Sink): void {
        this.router.put(uri, handler, undefined, undefined, sink);
    }
    /**
     * Register a 'post' endpoint.
     * @param uri The URI of the endpoint.
     * @param handler A handler func to invoke.
     * @param sink An optional response sink when returning an observable
     */
    post(uri: string, handler: Handler, sink?: Sink): void {
        this.router.post(uri, handler, undefined, undefined, sink);
    }
    /**
     * Register a 'del' (or delete) endpoint.
     * @param uri The URI of the endpoint.
     * @param handler A handler func to invoke.
     * @param sink An optional response sink when returning an observable
     */
    del(uri: string, handler: Handler, sink?: Sink): void {
        this.router.del(uri, handler, undefined, undefined, sink);
    }
    /**
     * Register an 'options' endpoint.
     * @param uri The URI of the endpoint.
     * @param handler A handler func to invoke.
     * @param sink An optional response sink when returning an observable
     */
    options(uri: string, handler: Handler, sink?: Sink): void {
        this.router.options(uri, handler, undefined, undefined, sink);
    }
    /**
     * Register a 'head' endpoint.
     * @param uri The URI of the endpoint.
     * @param handler A handler func to invoke.
     * @param sink An optional response sink when returning an observable
     */
    head(uri: string, handler: Handler, sink?: Sink): void {
        this.router.head(uri, handler, undefined, undefined, sink);
    }
    /**
     * Register a 'patch' endpoint.
     * @param uri The URI of the endpoint.
     * @param handler A handler func to invoke.
     * @param sink An optional response sink when returning an observable
     */
    patch(uri: string, handler: Handler, sink?: Sink): void {
        this.router.patch(uri, handler, undefined, undefined, sink);
    }

    /**
     * Apply the routes of this router to a given server.
     * @param service The name of the service
     * @param server The server to register the routes at
     * @param c A dependencies container.
     */
    apply(service: string, server: Server, c: Container): void {
        this.router.apply(service, server, c);
    }

}
