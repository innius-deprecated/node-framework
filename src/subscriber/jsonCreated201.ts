import {RequestContext, NotFoundError} from "@toincrease/node-context";
import {Observable, Subscription} from "@reactivex/rxjs";

/**
 * A JSON sink -- process response as JSON. 
 * 
 * Will return a 201 if the resulting observable ends succesfully.
 * 
 * @param source The source observable. Will emit everything on complete().
 * @param ctx The request context of the requestor.
 * @returns {Subscription} The subscription of the observable.
 */

const committed = "Response is already committed -- aborting";
const code = 201;

export function jsonCreated201Sink(source: Observable<any>, ctx: RequestContext): Subscription {
    var elements: any[] = [];
    var res = ctx.response();
    return source.subscribe(
        (value: any) => {
            elements.push(value);
        },
        (e: any) => {
            if (res.committed()) {
                res.logger().error(committed);
                return;
            } else {
                res.error(e);
            }
        },
        () => {
            if (res.committed()) {
                res.logger().error(committed);
                return;
            }
            if (elements.length === 0) {
                res.send("", code);
            } else if (elements.length === 1) {
                res.json(elements.pop(), code);
            } else {
                res.json(elements, code);
            }
        }
    );
}
