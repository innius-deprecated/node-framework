import {assert} from "chai";
import {jsonEmpty404Sink} from "./jsonEmpty404";
import {newTestRequestContextBuilder} from "@toincrease/node-context";

import {Observable} from "@reactivex/rxjs";
import {NotImplementedError} from "@toincrease/node-context";

describe("jsonEmpty404", () => {
    it("return a 404 on an empty observable", (done) => {
        let ctx = newTestRequestContextBuilder()
            .withCallback((err: any) => {
                assert.equal(ctx.testResponse().getStatus(), 404);
                assert.deepEqual(ctx.testResponse().getResponse(), {
                    code: "NotFoundError",
                    message: "Requested item not found"
                });
                done(err);
            })
            .build();
        jsonEmpty404Sink(Observable.empty(), ctx);
    });
    it("return a 200 on a non-empty observable", (done) => {
        let ctx = newTestRequestContextBuilder()
            .withCallback((err: any) => {
                assert.equal(ctx.testResponse().getStatus(), 200);
                assert.equal(ctx.testResponse().getResponse(), "a");
                done(err);
            })
            .build();
        jsonEmpty404Sink(Observable.of("a"), ctx);
    });
    it("return a thrown error", (done) => {
        let ctx = newTestRequestContextBuilder()
            .withCallback((err: any) => {
                assert.equal(ctx.testResponse().getStatus(), 501);
                assert.deepEqual(ctx.testResponse().getResponse(), {
                    code: "NotImplementedError",
                    message: "ABC"
                });
                done(err);
            })
            .build();
        jsonEmpty404Sink(Observable.throw(new NotImplementedError("ABC")), ctx);
    });
});
