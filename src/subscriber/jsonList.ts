import {RequestContext} from "@toincrease/node-context";
import {jsonSink} from "./json";
import {Observable, Subscription} from "@reactivex/rxjs";

/**
 * A JSON sink -- process response as JSON list.
 * @param source The source observable. Will emit everything on complete().
 * @param ctx The request context of the requestor.
 * @returns {Subscription} The subscription of the observable.
 */

const committed = "Response is already committed -- aborting";

export function jsonListSink(source: Observable<any>, ctx: RequestContext): Subscription {
    return jsonSink(source.toArray(), ctx);
}
