import {assert} from "chai";
import {jsonListSink} from "./jsonList";
import {newTestRequestContextBuilder} from "@toincrease/node-context";

import {Observable} from "@reactivex/rxjs";
import {NotImplementedError} from "@toincrease/node-context";

describe("jsonListSink", () => {
    it("return a 200 on an empty observable", (done) => {
        let ctx = newTestRequestContextBuilder()
            .withCallback((err: any) => {
                assert.equal(ctx.testResponse().getStatus(), 200);
                assert.deepEqual(ctx.testResponse().getResponse(), []);
                done(err);
            })
            .build();
        jsonListSink(Observable.empty(), ctx);
    });
    it("return a 200 list on a single element", (done) => {
        let ctx = newTestRequestContextBuilder()
            .withCallback((err: any) => {
                assert.equal(ctx.testResponse().getStatus(), 200);
                assert.deepEqual(ctx.testResponse().getResponse(), ["a"]);
                done(err);
            })
            .build();
        jsonListSink(Observable.of("a"), ctx);
    });
    it("return a 200 on a non-empty observable", (done) => {
        let ctx = newTestRequestContextBuilder()
            .withCallback((err: any) => {
                assert.equal(ctx.testResponse().getStatus(), 200);
                assert.deepEqual(ctx.testResponse().getResponse(), ["a", "b", "c"]);
                done(err);
            })
            .build();
        jsonListSink(Observable.of("a", "b", "c"), ctx);
    });
    it("return a thrown error", (done) => {
        let ctx = newTestRequestContextBuilder()
            .withCallback((err: any) => {
                assert.equal(ctx.testResponse().getStatus(), 501);
                assert.deepEqual(ctx.testResponse().getResponse(), {
                    code: "NotImplementedError",
                    message: "ABC"
                });
                done(err);
            })
            .build();
        jsonListSink(Observable.throw(new NotImplementedError("ABC")), ctx);
    });
});
