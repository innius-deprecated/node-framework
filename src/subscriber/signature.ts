import {Observable, Subscription} from "@reactivex/rxjs";
import {RequestContext} from "@toincrease/node-context";

export interface Sink {
    /**
     * A JSON sink -- process an observable handler.
     * @param source The source observable. Will emit everything on complete().
     * @param ctx The request context of the requestor.
     * @returns Subscription The subscription of the observable.
     */
    (source: Observable<any>, ctx: RequestContext): Subscription;
}
