import {assert} from "chai";
import {jsonSink} from "./json";
import {newTestRequestContextBuilder} from "@toincrease/node-context";

import {Observable} from "@reactivex/rxjs";
import {NotImplementedError} from "@toincrease/node-context";

describe("jsonSink", () => {
    it("return a 200 on an empty observable", (done) => {
        let ctx = newTestRequestContextBuilder()
            .withCallback((err: any) => {
                assert.equal(ctx.testResponse().getStatus(), 200);
                assert.deepEqual(ctx.testResponse().getResponse(), "");
                done(err);
            })
            .build();
        jsonSink(Observable.empty(), ctx);
    });
    it("return a 200 on a non-empty observable", (done) => {
        let ctx = newTestRequestContextBuilder()
            .withCallback((err: any) => {
                assert.equal(ctx.testResponse().getStatus(), 200);
                assert.equal(ctx.testResponse().getResponse(), "a");
                done(err);
            })
            .build();
        jsonSink(Observable.of("a"), ctx);
    });
    it("return a thrown error", (done) => {
        let ctx = newTestRequestContextBuilder()
            .withCallback((err: any) => {
                assert.equal(ctx.testResponse().getStatus(), 501);
                assert.deepEqual(ctx.testResponse().getResponse(), {
                    code: "NotImplementedError",
                    message: "ABC"
                });
                done(err);
            })
            .build();
        jsonSink(Observable.throw(new NotImplementedError("ABC")), ctx);
    });
});
