import {assert} from "chai";
import {jsonCreated201Sink} from "./jsonCreated201";
import {newTestRequestContextBuilder} from "@toincrease/node-context";

import {Observable} from "@reactivex/rxjs";
import {NotImplementedError} from "@toincrease/node-context";

describe("jsonCreated201", () => {
    it("return a 201 on an empty observable", (done) => {
        let ctx = newTestRequestContextBuilder()
            .withCallback((err: any) => {
                assert.equal(ctx.testResponse().getStatus(), 201);
                assert.deepEqual(ctx.testResponse().getResponse(), "");
                done(err);
            })
            .build();
        jsonCreated201Sink(Observable.empty(), ctx);
    });
    it("return a 201 on a non-empty observable", (done) => {
        let ctx = newTestRequestContextBuilder()
            .withCallback((err: any) => {
                assert.equal(ctx.testResponse().getStatus(), 201);
                assert.equal(ctx.testResponse().getResponse(), "a");
                done(err);
            })
            .build();
        jsonCreated201Sink(Observable.of("a"), ctx);
    });
    it("return a thrown error", (done) => {
        let ctx = newTestRequestContextBuilder()
            .withCallback((err: any) => {
                assert.equal(ctx.testResponse().getStatus(), 501);
                assert.deepEqual(ctx.testResponse().getResponse(), {
                    code: "NotImplementedError",
                    message: "ABC"
                });
                done(err);
            })
            .build();
        jsonCreated201Sink(Observable.throw(new NotImplementedError("ABC")), ctx);
    });
});
