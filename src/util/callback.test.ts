import {assert} from "chai";
import {AwaitCallback, Await} from "./callback";
import {newScopedLogger} from "@toincrease/node-logger";

describe("callback", () => {
    var log = newScopedLogger();

    describe("#continuation", () => {
        it("should continue", (done) => {
            let c = new Await(log, done);
            let cb: AwaitCallback = c;
            c.expect(1);
            c.ack();
            c.wrapperComplete();
        });
        it("should not be completed", () => {
            let c = new Await(log, assert.fail);
            c.expect(1);
            c.wrapperComplete();
            assert.isFalse(c.isCompleted());
        });
        it("should not be completed when given a partial ack.", () => {
            let c = new Await(log, assert.fail);
            c.expect(2);
            c.ack();
            c.wrapperComplete();
            assert.isFalse(c.isCompleted());
        });
        it("should complete after enough acks.", (done) => {
            let c = new Await(log, () => {
                assert.isTrue(c.isCompleted());
                done();
            });
            c.expect(2);
            c.wrapperComplete();
            c.ack();
            c.ack();
        });
        it("should be able to deal with a list of callbacks.", (done) => {
            var cnt = 0;
            let c = new Await(log, () => {
                assert.equal(3, cnt);
                done();
            });
            c.expect([
                () => cnt++,
                () => cnt++,
                () => cnt++
            ]);
            c.wrapperComplete();
        });
        it("Tasks can throw.", (done) => {
            var cnt = 0;
            var c = new Await(log, () => {
                assert.equal(2, cnt);
                assert.isTrue(c.isCompleted());
                done();
            });
            c.expect([
                () => cnt++,
                () => { throw new Error("abc"); },
                () => cnt++,
            ]);
            c.wrapperComplete();
        });
        it("should embed within the framework.", (done) => {
            let c = new Await(log, done);
            c.expect(1);
            c.ack();
            assert.isFalse(c.isCompleted());
            c.wrapperComplete();
        });
        it("should be able to add an expectation and dynamically adjust.", (done) => {
            let c = new Await(log, done);
            c.expect(2);
            c.wrapperComplete();

            c.ack();
            c.addExpectation(1);
            c.ack();
            assert.isFalse(c.isCompleted());
            c.addExpectation(3);
            c.ack();
            c.ack();
            c.ack();
            assert.isFalse(c.isCompleted());
            c.ack();
            assert.isTrue(c.isCompleted());
        });

        it("should not fire twice.", (done) => {
            let c = new Await(log, done);
            c.expect(2);
            c.wrapperComplete();

            c.ack();
            c.ack();
            assert.isTrue(c.isCompleted());
            c.addExpectation(-1);
            c.ack();
            assert.isTrue(c.isCompleted());
        });
    });
});
