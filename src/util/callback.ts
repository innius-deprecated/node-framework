import {Logger} from "@toincrease/node-logger";

export class Await implements AwaitCallback {
    private amount: number = 0;
    private cnt: number = 0;
    private wrapperCompleted: boolean;
    private hasFired: boolean;

    constructor(private log: Logger, private cb: () => void) {
        this.wrapperCompleted = false;
        this.hasFired = false;
    }

    /**
     * Provide the expected number of acknowledgements before continuing.
     * @param amountOrTasks The expected number of tasks to complet, or an array of the tasks itself.
     */
    expect(amountOrTasks?: number | (() => void)[]): void {
        this.cnt = 0;
        if (Array.isArray(amountOrTasks)) {
            let tasks = <[(() => void)]>amountOrTasks;
            this.amount = tasks.length;
            tasks.forEach((t) => {
                try {
                    t();
                } catch (e) {
                    this.log.error(e);
                }
                this.ack();
            });
        } else {
            this.amount = <number>amountOrTasks;
        }
    }
    addExpectation(num: number): void {
        if (!this.hasFired) {
            this.amount = this.amount + num;
            this.maybeFire();
        }
    }

    /**
     * Signal that the wrapper func is complete.
     */
    wrapperComplete(): void {
        this.wrapperCompleted = true;
        this.maybeFire();
    }

    /**
     * Acknowledge something. As soon os the number of acks is equal to the expected amount, the continuation callback
     * is executed.
     */
    ack(): void {
        this.cnt++;
        this.maybeFire();
    }

    /**
     * Check whether this AwaitCallback task has completed successfully.
     * @returns {boolean}
     */
    isCompleted(): boolean {
        if (this.hasFired) {
            return this.hasFired;
        } else {
            let acks = this.cnt === this.amount;
            return acks && this.wrapperCompleted;
        }
    }

    private maybeFire(): void {
        if (this.isCompleted() && !this.hasFired) {
            this.hasFired = true;
            this.cb();
        }
    }
}

export interface AwaitCallback {

    /**
     * Provide the expected number of acknowledgements before continuing.
     * @param amountOrTasks The expected number of tasks to complet, or an array of the tasks itself.
     */
    expect(amountOrTasks?: number | (() => void)[]): void;

    /**
     * Given that the callback hasn't fired yet, adjust the amount of expectations without resetting the current count
     * @param num The number of expectations to add or subtract.
     */
    addExpectation(num: number): void;

    /**
     * Acknowledge something. As soon os the number of acks is equal to the expected amount, the continuation callback
     * is executed.
     */
    ack(): void;

    /**
     * Check whether this AwaitCallback task has completed successfully.
     * @returns {boolean}
     */
    isCompleted(): boolean;
}
