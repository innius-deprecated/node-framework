var AWS = require("aws-sdk"); // to typings

import {Container} from "../dependencies/Container";
import {Observable, Observer} from "@reactivex/rxjs";
import {TaskContext} from "@toincrease/node-context";

export class DynamoDB {
    private _dynamodb: any;
    constructor(region: string, endpoint: string) {
        this._dynamodb = new AWS.DynamoDB({ region: region, endpoint: endpoint });
    }

    describeTable(tableName: string): Observable<any> {
        const dyndb = this._dynamodb;

        return Observable.create((o: Observer) => {
            let params = {
                TableName: tableName
            };
            dyndb.describeTable(params, (err, data) => {
                if (err) {
                    o.error(err);
                } else {
                    o.next(data);
                    o.complete();
                }
            });
        });
    }
}

export interface Subscription {
    SubscriptionArn: string;
    Owner: string;
    Protocol: string;
    Endpoint: string;
    TopicArn: string;
}

export class SNS {
    private _sns: any;
    constructor(region: string) {
        this._sns = new AWS.SNS({ region: region });
    }

    describeTopic(c: TaskContext, topicArn: string): Observable<any> {
        const sns = this._sns;

        return Observable.create((o: Observer) => {
            let params = { TopicArn: topicArn };

            sns.getTopicAttributes(params, (err, data) => {
                if (err) {
                    o.error(err);
                } else {
                    o.next(data);
                    o.complete();
                }
            });
        });
    }

    listSubscriptionsByTopic(c: TaskContext, topicArn: string): Observable<Subscription> {
        const sns = this._sns;

        return Observable.create((o: Observer) => {
            let params = { TopicArn: topicArn };

            sns.listSubscriptionsByTopic(params, (err, data: { Subscriptions: Subscription[] }) => {
                if (err) {
                    o.error(err);
                } else {
                    data.Subscriptions.forEach(x => o.next(x));
                    o.complete();
                }
            });
        });
    }
}

export class AWSProxy {
    sns: SNS;
    dynamodb: DynamoDB;

    constructor(d: Container) {
        let region = d.configuration().region;
        let dynamodb_endpoint = d.configuration().dynamodb_endpoint;
        this.sns = new SNS(region.string());
        let db = dynamodb_endpoint.value();
        if (db === "") {
            this.dynamodb = new DynamoDB(region.string(), undefined);
        } else {
            this.dynamodb = new DynamoDB(region.string(), db);
        }
    }
}
