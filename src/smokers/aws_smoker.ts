import {TaskContext} from "@toincrease/node-context";
import {Container} from "../dependencies/Container";

import {Observable} from "@reactivex/rxjs";
import {ServiceSmoke} from "../endpoints/smoke";
import {blackSmoke, whiteSmoke} from "./service_smoker";

import {AWSProxy} from "./aws_proxy";

/**
 * Create an AWS smoker. Makes smaketesting AWS components trivial.
 * @param d The container for dependency management.
 * @returns {AWSSmoker} A preconfigured AWS smoker.
 */
export function awsSmoker(d: Container): AWSSmoker {
    return new AWSSmoker(new AWSProxy(d));
}

const timeout = 1500;

export class AWSSmoker {
    constructor(private aws: AWSProxy) { }

    /**
     * Smoketest for a DynamoDB instance / connection.
     * @param c The TaskContext for this test.
     * @param tableName The table to verify.
     * @returns {Observable<ServiceSmoke>}
     */
    dynamodb(c: TaskContext, tableName: string): Observable<ServiceSmoke> {
        const smoker = "dynamodb";
        return this.aws.dynamodb.describeTable(tableName)
            .map(x => whiteSmoke(smoker, tableName))
            .timeout(timeout)
            .catch((err) => Observable.of(blackSmoke(smoker, tableName, err)));
    }

    /**
     * Smoketest for an SNS topic.
     * @param c The TaskContext for this test.
     * @param topicArn The topicARN to verify.
     * @returns {Observable<ServiceSmoke>}
     */
    snsTopic(c: TaskContext, topicArn: string): Observable<ServiceSmoke> {
        const smoker = "snsTopic";
        return this.aws.sns.describeTopic(c, topicArn)
            .map(x => whiteSmoke(smoker, topicArn))
            .timeout(timeout)
            .catch((err) => Observable.of(blackSmoke(smoker, topicArn, err)));
    }

    /**
     * Smoketest for an SNS Topic Subscription
     * @param c The TaskContext for this test.
     * @param topicArn The table to verify.
     * @returns {Observable<ServiceSmoke>}
     */
    snsTopicSubscription(c: TaskContext, topicArn: string): Observable<ServiceSmoke> {
        const smoker = "snsTopicSubscription";
        return this.aws.sns.listSubscriptionsByTopic(c, topicArn)
            .filter(x => x.Protocol === "https")
            .map(x => whiteSmoke(smoker, topicArn))
            .defaultIfEmpty(blackSmoke(smoker, topicArn, new Error("no https subscription found for the sns topic")))
            .timeout(timeout)
            .catch((err) => Observable.of(blackSmoke("snsTopic", topicArn, err)));
    }
}
