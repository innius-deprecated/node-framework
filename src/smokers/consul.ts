import {resolve} from "dns";
import {TaskContext} from "@toincrease/node-context";
import {NetworkHttp} from "@toincrease/node-http";
import {blackSmoke, whiteSmoke} from "./service_smoker";

import {Observable, Observer} from "@reactivex/rxjs";
import {ServiceSmoke} from "../endpoints/smoke";
import {http_timeout} from "../constants/constants";

function getConsulMasterAddress():Observable<string> {
    return Observable.create((observer: Observer) => {
        if (observer.isUnsubscribed) {
            return;
        }
        resolve("consul.service.consul", (err, addr) => {
            if (err) {
                observer.error(err);
            } else {
                if (addr.length > 0) {
                    observer.next(addr[0]);
                    observer.complete();
                } else {
                    observer.error(new Error("No records found for consul."));
                }
            }
        });
    });
}

export function ConsulMasterSmoker(c:TaskContext):Observable<ServiceSmoke> {
    let svc = "consul-master";
    let http = new NetworkHttp();
    return getConsulMasterAddress()
        .flatMap((ip) => http.get(`http://${ip}:8500/v1/status/leader`))
        .map((leader) => whiteSmoke(svc, `A ${svc} is alive, leader: ${leader}`))
        .timeout(http_timeout)
        .catch((err) => Observable.of(blackSmoke(svc, svc, err)));
}

export function ConsulAgentSmoker(c:TaskContext):Observable<ServiceSmoke> {
    let svc = "consul-agent";
    return getConsulMasterAddress()
        .map((ip) => whiteSmoke(svc, `A ${svc} is alive at ${ip}`))
        .timeout(http_timeout)
        .catch((err) => Observable.of(blackSmoke(svc, svc, err)));
}
