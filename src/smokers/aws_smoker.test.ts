/// <reference path="../../node_modules/typemoq/typemoq.node.d.ts" />
import {assert} from "chai";
import {Mock, It} from "typemoq";
import {Observable} from "@reactivex/rxjs";

import {newTaskContext} from "@toincrease/node-context";
import {MutableContainer} from "../dependencies/MutableContainer";

import {AWSSmoker} from "./aws_smoker";
import {AWSProxy, DynamoDB, SNS, Subscription} from "./aws_proxy";
import {ServiceSmoke} from "../endpoints/smoke";
import {MemoryConfigurator} from "@toincrease/node-config";
import {newScopedLogger} from "@toincrease/node-logger";
import {Await} from "../util/callback";
import {newConfiguration} from "../dependencies/configuration";

describe("AWSSmoker", () => {
    let name = "name";
    let ctx = newTaskContext({});
    let d = new MutableContainer("");
    let l = newScopedLogger();
    let c = new MemoryConfigurator();
    c.set("region", "eu-west-1");
    c.set("dynamodb_endpoint", "abc");
    c.set(`${name}/EnvironmentStackName`, "xyz");
    let w = new Await(l, () => {
        return;
    });
    d.setConfiguration(newConfiguration(name, l, w, c));
    w.wrapperComplete();

    describe("#dynamodb", () => {
        let dynamodb = new DynamoDB("", "");
        let proxy = new AWSProxy(d);
        let smoker = new AWSSmoker(proxy);
        let mock = Mock.ofInstance(dynamodb);
        proxy.dynamodb = mock.object;

        mock.setup(x => x.describeTable(It.isAny())).returns(x => Observable.of({}));

        let smoke: ServiceSmoke;
        it("should give smoke", (done) => {
            smoker.dynamodb(ctx, "my_table").subscribe(x => smoke = x, done, done);
        });

        it("should give white smoke", () => {
            assert.equal(smoke.status, 200);
        });

        describe("with error", () => {
            before(() => {
                mock.setup(x => x.describeTable(It.isAny())).returns(x => Observable.throw(new Error("dynamodb error")));
            });

            let smoke: ServiceSmoke;
            it("should give smoke", (done) => {
                smoker.dynamodb(ctx, "my_table").subscribe(x => smoke = x, done, done);
            });

            it("should give black smoke", () => {
                assert.equal(smoke.status, 500);
            });
        });
    });

    describe("#sns", () => {
        let sns = new SNS("");
        let proxy = new AWSProxy(d);
        let smoker = new AWSSmoker(proxy);
        let mock = Mock.ofInstance(sns);
        proxy.sns = mock.object;

        describe("topic", () => {
            before(() => {
                mock.setup(x => x.describeTopic(It.isAny(), It.isAny())).returns(x => Observable.of({}));
            });
            let smoke: ServiceSmoke;
            it("should give smoke", (done) => {
                smoker.snsTopic(ctx, "topic_arn").subscribe(x => smoke = x, done, done);
            });
            it("should give white smoke", () => {
                assert.equal(smoke.status, 200);
            });

            describe("with error", () => {
                before(() => {
                    mock.setup(x => x.describeTopic(It.isAny(), It.isAny())).returns(x => Observable.throw(new Error("some error")));
                });
                let smoke: ServiceSmoke;
                it("should give smoke", (done) => {
                    smoker.snsTopic(ctx, "topic_arn").subscribe(x => smoke = x, done, done);
                });
                it("should give black smoke", () => {
                    assert.equal(smoke.status, 500);
                });
            });
        });
        describe("topicSubscription", () => {
            let subscription: Subscription = {
                SubscriptionArn: "some arn",
                Owner: "",
                Protocol: "https",
                Endpoint: "",
                TopicArn: ""
            };
            before(() => {
                mock.setup(x => x.listSubscriptionsByTopic(It.isAny(), It.isAny())).returns(x => Observable.of(subscription));
            });
            let smoke: ServiceSmoke;

            it("should give smoke", (done) => {
                smoker.snsTopicSubscription(ctx, "topic_arn").subscribe(x => smoke = x, done, done);
            });

            it("should give white smoke", () => {
                assert.equal(smoke.status, 200);
            });

            describe("no subscriptions found", () => {
                before(() => {
                    subscription.Protocol = "ftp";
                    mock.setup(x => x.listSubscriptionsByTopic(It.isAny(), It.isAny())).returns(x => Observable.of(subscription));
                });
                let smoke: ServiceSmoke;

                it("should give smoke", (done) => {
                    smoker.snsTopicSubscription(ctx, "topic_arn").subscribe(x => smoke = x, done, done);
                });

                it("should give black smoke", () => {
                    assert.equal(smoke.status, 500);
                });
            });

            describe("with error", () => {
                before(() => {
                    mock.setup(x => x.listSubscriptionsByTopic(It.isAny(), It.isAny())).returns(x => Observable.throw(new Error("")));        
                });
                let smoke: ServiceSmoke;

                it("should give smoke", (done) => {
                    smoker.snsTopicSubscription(ctx, "topic_arn").subscribe(x => smoke = x, done, done);
                });

                it("should give black smoke", () => {
                    assert.equal(smoke.status, 500);
                });
            });
        });

    });
});
