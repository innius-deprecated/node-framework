export enum MicroService {
    AnalyticsManagementService = "analytics-management-service",
    AnalyticsQueryService = "analytics-query-service",
    AuthenticationService = "authentication-service",
    AuthorizationService = "authorization-service",
    CollaborationService = "collaboration-service",
    CollateralService = "collateral-service",
    ConnectionService = "connection-service",
    DemomachineService = "demomachine-service",
    DevopsDashboardService = "devops-dashboard-service",
    FeedbackService = "feedback-service",
    IdentificationService = "identification-service",
    KpiService = "kpi-service",
    LocationService = "location-service",
    MessagingService = "messaging-service",
    MachinePartsService = "parts-service",
    SensorService = "sensor-service",
    SpecificationService = "specification-service",
    StatusService = "status-service",
    UploadService = "upload-service",
    WebUiAdminService = "web-ui-admin-service",
    ActivityService = "activity-service"
}


/**
 * A list of all the addable services.
 *
 * Adding a name here will automatically register it for health-, and smoke tests.
 *
 * @returns {string[]} All services.
 */
export function services(): MicroService[] {
    return [
        MicroService.ActivityService,
        MicroService.AnalyticsManagementService,
        MicroService.AnalyticsQueryService,
        MicroService.AuthenticationService,
        MicroService.AuthorizationService,
        MicroService.CollaborationService,
        MicroService.CollateralService,
        MicroService.ConnectionService,
        MicroService.DemomachineService,
        MicroService.DevopsDashboardService,
        MicroService.FeedbackService,
        MicroService.IdentificationService,
        MicroService.KpiService,
        MicroService.LocationService,
        MicroService.MessagingService,
        MicroService.MachinePartsService,
        MicroService.SensorService,
        MicroService.SpecificationService,
        MicroService.StatusService,
        MicroService.UploadService,
        MicroService.WebUiAdminService
    ];
}
