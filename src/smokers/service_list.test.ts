import {assert} from "chai";
import {MicroService} from "./service_list";

describe("#ServiceList", () => {
    it("should be an enum string", () => {
        assert.equal(MicroService.SensorService, "sensor-service");
    });
});
