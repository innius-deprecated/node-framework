import {Observable} from "@reactivex/rxjs";

import {Http, NetworkHttp} from "../proxy/http";
import {ServiceSmoke, statusInternalServerError} from "../endpoints/smoke";
import {TaskContext} from "@toincrease/node-context";
import {MicroService} from "./service_list";
import {smoke_interval} from "../constants/constants";

/**
 * Create a new and preconfigured ServiceSmoker object.
 * This can be used to verify whether a downstream service is available.
 * @returns {ServiceSmoker}
 */
export function serviceSmoker(): ServiceSmoker {
    return new ServiceSmoker(new NetworkHttp());
}

export class ServiceSmoker {
    private _http: Http;
    private _proto: string;
    private _port: number;
    private _timeout: number;

    constructor(http: Http, timeout: number = 200) {
        this._http = http;
        this._proto = "http://";
        this._port = 8080;
        this._timeout = timeout;
    }

    private getHost(serviceName: string): string {
        return serviceName + ".service.consul";
    }

    private getPort(): number {
        return this._port;
    }

    private getAddress(serviceName: string): string {
        return `${this._proto}${this.getHost(serviceName)}:${this.getPort()}`;
    }

    /**
     * Verify whether a downstream service is available.
     * @param c The taskcontext used to log.
     * @param serviceName The service to verify.
     * @returns {Observable<ServiceSmoke>}
     */
    smoke(c: TaskContext, serviceName: MicroService|MicroService[]): Observable<ServiceSmoke> {
        if(serviceName instanceof Array) {
            let that = this;
            return Observable
                .from(serviceName)
                .flatMap((s) => that.smoke(c, s));
        } else {
            return this._http
                .get(this.getAddress(<string>serviceName) + "/smoketest")
                .map((x) => <ServiceSmoke>JSON.parse(x))
                .map((x: ServiceSmoke) => {
                    let delta = Date.now() - x.timestamp;
                    if (x.timestamp !== 0 && delta > 2*smoke_interval) {
                        x.status = statusInternalServerError;
                        x.message = `Check expired (executed ${delta} ago); original message: ${x.message}`;
                    }
                    return x;
                })
                .timeout(this._timeout)
                .catch((e: Error) => {
                    return Observable.of(
                        blackSmoke("service", <string>serviceName, new Error(`the smoke endpoint is not available: ${e}`)));
                });
        }
    }
}

/**
 * Black smoke (e.g. svc down)
 * @param service
 * @param resource
 * @param err
 * @returns {{name: string, version: string, buildtime: string, status: number, message: string, hostname: string, timestamp: number}}
 */
export function blackSmoke(service: string, resource: string, err: Error): ServiceSmoke {
    return {
        name: resource,
        version: "",
        buildtime: "",
        status: 500,
        message: `${service} smoke test for ${resource} failed`,
        hostname: "",
        ipaddress: "",
        timestamp: Date.now()
    };
}

export function whiteSmoke(name: string, message: string): ServiceSmoke {
    return {
        name: name,
        version: "",
        buildtime: "",
        status: 200,
        message: message,
        hostname: "",
        ipaddress: "",
        timestamp: Date.now()
    };
}
