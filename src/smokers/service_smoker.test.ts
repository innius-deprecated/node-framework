import {assert} from "chai";

import {newHttpBuilder} from "../proxy/http";

import {ServiceSmoker} from "./service_smoker";

import {newTaskContext} from "@toincrease/node-context";
import {MicroService} from "./service_list";
import {smoke_interval} from "../constants/constants";

describe("#ServiceSmoker", () => {
    let ctx = newTaskContext({});
    let t = Date.now();
    let whiteSmoke = {
        "name": "sensor-service",
        "version": "bfc39c8336e86a150f708c0609169e02fd04ee67",
        "buildtime": "2016-04-12T14:23:38.852Z",
        "status": 200,
        "message": "Service seems to be fine",
        "hostname": "C107",
        "timestamp": t,
        "dependents": [
            {
                "name": "logentries_local",
                "version": "",
                "buildtime": "",
                "status": 200,
                "message": "ACTIVE",
                "hostname": "",
                "timestamp": t
            },
            {
                "name": "LogReceivedEventSNSTopic",
                "version": "",
                "buildtime": "",
                "status": 200,
                "message": "",
                "hostname": "",
                "timestamp": t
            }
        ]
    };

    it("should give smoke for the service and its dependents", (done) => {
        let http = newHttpBuilder()
            .withGet()
            .withStringifiedBody(whiteSmoke)
            .buildResponse()
            .build();

        let c = new ServiceSmoker(http);

        c.smoke(ctx, MicroService.SensorService).subscribe(x => {
            assert.equal(x.name, "sensor-service");
            assert.equal(x.status, 200);
            assert.lengthOf(x.dependents, 2);
            done();
        });
    });

    it("should give smoke for the service and its dependents via a list", (done) => {
        let http = newHttpBuilder()
            .withGet()
            .withStringifiedBody(whiteSmoke)
            .buildResponse()
            .build();

        let c = new ServiceSmoker(http);
        let list = [];
        list.push(MicroService.SensorService);

        c.smoke(ctx, list).subscribe(x => {
            assert.equal(x.name, "sensor-service");
            assert.equal(x.status, 200);
            assert.lengthOf(x.dependents, 2);
            done();
        });
    });

    it("service error should return black smoke", (done) => {
        let http = newHttpBuilder()
            .withGet()
            .withError(new Error("http error"))
            .buildResponse()
            .build();

        let c = new ServiceSmoker(http);

        c.smoke(ctx, MicroService.SensorService).subscribe(x => {
            assert.equal(x.name, "sensor-service");
            assert.equal(x.status, 500);
            done();
        });
    });

    it("service with a timeout should return black smoke", (done) => {
        let http = newHttpBuilder()
            .withGet()
            .withDelay(20)
            .buildResponse()
            .build();

        let c = new ServiceSmoker(http, 10);

        c.smoke(ctx, MicroService.SensorService).subscribe(x => {
            assert.equal(x.name, "sensor-service");
            assert.equal(x.status, 500);
            done();
        });
    });
    it("service with an outdated dependency should return black smoke", (done) => {
        let white2 = whiteSmoke;
        white2.timestamp = white2.timestamp - 3*smoke_interval;
        let http = newHttpBuilder()
            .withGet()
            .withStringifiedBody(white2)
            .buildResponse()
            .build();
        
        let c = new ServiceSmoker(http);
        let list = [MicroService.SensorService];
        
        c.smoke(ctx, list).subscribe(x => {
            assert.equal(x.name, "sensor-service");
            assert.equal(x.status, 500);
            done();
        });
    });
});
