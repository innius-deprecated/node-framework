
export const default_port = 8080;
export const default_properties_file = "config.properties";
export const default_consul_address = "172.17.0.1";

export const http_timeout = 1000; // ms
const second = 1000;
export const smoke_interval = 150 * second; // ms
