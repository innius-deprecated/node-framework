import {MutableContainer} from "../dependencies/MutableContainer";
import {Container} from "../dependencies/Container";
import {newConfiguration, Configuration} from "../dependencies/configuration";

import {Router} from "../router/Router";
import {ProtectedRouter, ProtectedRouterImpl} from "../router/protected_router";

import {health} from "../endpoints/health";
import {smoke, ServiceSmoke} from "../endpoints/smoke";
import {StaticRoute} from "../endpoints/static";

import {AwaitCallback, Await} from "../util/callback";

import {default_port, default_properties_file, default_consul_address} from "../constants/constants";

import * as cluster from "cluster";
import {cpus, hostname} from "os";
import {cwd, exit} from "process";

import {Logger} from "@toincrease/node-logger";
import {newTaskContext, TaskContext} from "@toincrease/node-context";
import {Configurator, PropertiesConfigurator, ConsulConfigurator} from "@toincrease/node-config";
import {initialize as metrics_initialize} from "@toincrease/node-metrics";

import {Observable} from "@reactivex/rxjs";
import * as restify from "restify";

const corsMiddleware = require("restify-cors-middleware");
import {authentication, authorization} from "@toincrease/node-sdk";
import {PrivateRouterImpl} from "../router/private_router";
import {AdminConfig, RegisterAdminSuite} from "../admin/admin";

/**
 * Execute the microservice in cluster mode, if defined as a local service.
 * @param ctx The task context for this task.
 * @param conf A configuration object.
 * @param f The continuation function to execute within the cluster.
 * @returns {any} A function that possibly configures a cluster.
 */
function _cluster(ctx:TaskContext, conf:Configuration, f:() => void):() => void {
    if (conf.local.value()) {
        // Local mode is for dev, don't do anything special.
        ctx.logger().debug("Ignoring cluster mode -- local mode detected.");
        return f;
    } else {
        ctx.logger().debug("Preparing cluster mode...");
        // Otherwise we need to execute in cluster mode.
        return () => {
            const numCPUs = cpus().length;

            if (cluster.isMaster) {
                // Fork a bunch of workers. Every process lives in isolation, so every phase happens numCPUs times as well.
                for (var i = 0; i < numCPUs; i++) {
                    cluster.fork();
                }

                cluster.on("exit", (worker, code, signal) => {
                    ctx.logger().info(`worker ${worker.process.pid} died with code ${code} (signal: ${signal})`);
                });
            } else {
                f();
            }
        };
    }
}

export abstract class Service {
    // All these properties are private -- they are injected into the func args if required for a given
    // initialization phase.
    private _name:string;
    private _version:string;
    private _buildtime:string;
    private context:TaskContext;
    private logger:Logger;
    private configuration:Configuration;

    constructor(name:string, version:string, buildtime:string) {
        if (!name) {
            name = "";
        }
        if (!version) {
            version = "";
        }
        if (!buildtime) {
            buildtime = "";
        }

        this._name = name;
        this._version = version;
        this._buildtime = buildtime;

        this.context = newTaskContext({
            name: name,
            hostname: hostname()
        });
        this.logger = this.context.logger();
    }

    /**
     * Start serving this microservice for fun and profit.
     */
    serve():void {
        var cfgs: Configurator[] = [];
        let continuation = () => {
            let cfgwaiter = new Await(this.logger, this.afterConfigurationCompleted.bind(this));
            this.configuration = newConfiguration(this.name(), this.logger, cfgwaiter, ...cfgs);
            cfgwaiter.wrapperComplete();
        };
        let initwaiter = new Await(this.logger, continuation);
        this.configurators(this.context, initwaiter)
            .toArray()
            .subscribe(
                (c) => {
                    this.logger.debug("Configurators are completely configured.");
                    cfgs = c;
                },
                (e) => {
                    this.logger.error("An error occurred in serve(): ");
                    this.logger.error(e);
                    exit(1); // It makes no sense to continue
                },
                () => {
                    initwaiter.wrapperComplete();
                }
            );
    }

    /**
     * This code can only be run after the config is completely initialized.
     */
    afterConfigurationCompleted(): void {
        this.logger.setLevel(this.configuration.loglevel.value());
        const router = new ProtectedRouterImpl();
        const rwcontainer = new MutableContainer(this.name());
        rwcontainer.setConfiguration(this.configuration);

        const that = this;

        this.configuration.getMyPrivateIpv4().subscribe(
            (ip: string) => {
                let sip = "ip-" + ip.replace(/\./g, "-");
                metrics_initialize(`${that.name()}.${sip}.`, ip);
                return;
            },
            (e) => that.logger.error(e)
        );

        // The continuation callback of the bootstrap procedure.
        var continuation = _cluster(this.context, this.configuration, () => {
            const container = rwcontainer.asContainer();

            var server = restify.createServer({
                name: that.name(),
                version: "0.0.0",
            });
            server.use(restify.plugins.acceptParser(server.acceptable));
            server.use(restify.plugins.queryParser());
            server.use(restify.plugins.bodyParser());
            server.pre(restify.pre.sanitizePath());
            
            const cors = corsMiddleware({                                
                origins: ["http://docs.innius.com", "https://webapp.dev.innius.com", "https://webapp.test.innius.com", "https://webapp.demo.innius.com", "https://webapp.live.innius.com", "https://webapp.innius.com"],
                allowCredentials : true, 
                allowHeaders:  ["Origin","Content-Type","Accept","Location","Link","Authorization"],
                exposeHeaders: ["Origin","Content-Type","Accept","Location","Link","Authorization"]
              });

            server.pre(cors.preflight);
            server.use(cors.actual);

            // Registering meta routes (smoke & health):
            that.logger.debug("Registering meta routes");
            let meta = new ProtectedRouterImpl();
            let meta_r = new Router(meta);
            meta_r.get("/health", health(that));
            that.configuration
                .getMyPrivateIpv4()
                .do(<any>{ error: (err: any): void => {
                    that.logger.error("An error occurred while retrieving private ipv4:");
                    that.logger.error(err);
                }})
                .catch((err: any) => Observable.of("unknown"))
                .take(1)
                .subscribe(
                    (ip) => {
                        meta_r.get("/smoketest", smoke(that, ip, () => that.smokers(that.context, container)));
                    },
                    (e) => {
                        // should not happen
                        that.logger.error(e);
                    },
                    () => {
                        meta.apply(that.name(), server, container);
                        that.logger.debug("Registering static routes");
                        var routes = that.staticHandlers(that.context);
                        routes.forEach((r:StaticRoute) => {                            
                            that.logger.info(`Registering path ${r.path} as static route ${r.uri}.`);
                            server.get(r.uri, restify.plugins.serveStatic({
                                directory: r.path,
                                default: r.default_file
                            }));
                        });

                        
                        that.logger.debug("Registering public handlers");
                        let p = new Router(router);
                        that.publicHandlers(that.context, p);

                        that.logger.debug("Registering protected handlers");
                        that.protectedHandlers(that.context, router);

                        that.logger.debug("Registering private handlers");
                        const pr = new PrivateRouterImpl();
                        that.privateHandlers(that.context, new Router(pr));

                        that.logger.debug("Registering admin suite");
                        let acfg = <AdminConfig>{
                            router: new Router(pr),
                            logger: that.logger
                        };
                        RegisterAdminSuite(acfg);

                        that.logger.debug("Applying router configuration");
                        p.apply(that.name(), server, container);
                        router.apply(that.name(), server, container);
                        pr.apply(that.name(), server, container);

                        that.logger.debug("Invoking before serve hook");
                        that.beforeServe(that.context, container);

                        that.logger.debug("Starting to listen...");
                        server.listen(default_port, () => that.logger.info(`${server.url} listening at ${server.url}`));
                    }
                );
        });

        this.context.logger().debug("Registering dependencies");
        let waiter = new Await(this.logger, continuation);

        // Register authorization and authentication clients as dependencies
        let ac = new authorization.AuthorizationClient(this.configuration.authorization_endpoint.string());
        rwcontainer.register(authorization.AuthorizationClient, ac);
        let bc = new authentication.AuthenticationClient(this.configuration.authentication_endpoint.string());
        rwcontainer.register(authentication.AuthenticationClient, bc);

        // Register dependencies, continuing with the declared continuation on completion.
        this.registerDependencies(this.context, rwcontainer, waiter);
        waiter.wrapperComplete();
    }

    /**
     * Reload this microservice. This is NOT IMPLEMENTED YET.
     * @returns {string} TODO
     */
    reload():any {
        this.logger.info("Reload called -- not implemented yet.");
        return "TODO";
    }

    /**
     * Return the name of this microservice.
     * @returns {string} The name of the microservice.
     */
    name():string {
        return this._name;
    }

    /**
     * Return the version of this microservice.
     * @returns {string} The version (git hash) of the microservice.
     */
    version():string {
        return this._version;
    }

    /**
     * Return the build time of this microservice.
     * @returns {string} The build time of the microservice.
     */
    buildTime():string {
        return this._buildtime;
    }

    /**
     * Register dependencies of the given microservice.
     * @param c TaskContext for this task.
     * @param d A mutable dependencies container to register dependencies.
     * @param w A waiter that will block execution until it is signalled as complete.
     */
    registerDependencies(c:TaskContext, d:MutableContainer, w:AwaitCallback):void {
        c.logger().info("Default registerDependencies() called");
        w.expect(1);
        w.ack();
        return;
    }

    /**
     * Configure dynamic configuration configurators.
     * @param c The taskcontext of this task.
     * @param w A waiter that will block execution until it is signalled as complete.
     * @returns {PropertiesConfigurator[]}
     */
    configurators(c:TaskContext, w:AwaitCallback):Observable<Configurator> {
        c.logger().info("default configurators() called");
        return Observable.create((sub) => {
            let props = new PropertiesConfigurator(c.logger(), default_properties_file);
            let conf: Configuration;
            let continuation = () => {
                if (!conf.local || !conf.local.value()) {
                    c.logger().info("Not in local mode or provided with an invalid config file -- enabling consul configuration source.");
                    sub.next(new ConsulConfigurator(c.logger(), default_consul_address));
                } else {
                    c.logger().info("In local mode -- skipping consul configuration source.");
                }
                sub.next(props);
                sub.complete();
                w.ack();
            };
            let waiter = new Await(this.logger, continuation);
            conf = newConfiguration(this.name(), c.logger(), waiter, props);
            waiter.wrapperComplete();
        });
    }

    /**
     * Register static handlers. The given directory will be served at provided url.
     * By default, this will serve the "/apidocs" route a folder with the same name.
     * @example let docs = new StaticRoute(new RegExp("^/apidocs/.*"), "/docs/");
     * @param c The task context for this task.
     */
    staticHandlers(c:TaskContext):StaticRoute[] {
        c.logger().info("default staticHandlers() called");
        let docs = new StaticRoute(new RegExp("^/apidocs(:?/.*)?"), cwd());
        docs.default_file = "index.html";
        return [docs];
    }

    /**
     * Register private handlers of the given microservice.
     * These handlers can only be invoked from within the VPC.
     * NOTE: THIS IS DONE BY CHECKING IF THE HEADER X-Forward-For is missing.
     * If present, it is assumed to be originating from outside our VPC.
     * @param c The task context for this task.
     * @param r The router to register routes with.
     */
    privateHandlers(c:TaskContext, r:Router):void {
        c.logger().info("default publicHandlers() called");
        return;
    }

    /**
     * Register public handlers of the given microservice.
     * @param c The task context for this task.
     * @param r The router to register routes with.
     */
    publicHandlers(c:TaskContext, r:Router):void {
        c.logger().info("default publicHandlers() called");
        return;
    }

    /**
     * Register secured handlers for the given microservice.
     * @param c The task context of this task.
     * @param r The secured router to register routes with.
     */
    protectedHandlers(c:TaskContext, r:ProtectedRouter):void {
        c.logger().info("default securedHandlers() called");
        return;
    }

    /**
     * A hook to execute any pre-serve tasks in.
     * @param c The task context of this task.
     * @param d The dependencies container.
     */
    beforeServe(c:TaskContext, d:Container):void {
        c.logger().info("default beforeServe() called");
        return;
    }

    /**
     * The smokers for this microservice.
     * @param c The task context of this task.
     * @param d The dependencies container.
     * @returns {Observable<ServiceSmoke>} An observable with the service smoke results.
     */
    smokers(c:TaskContext, d:Container):Observable<ServiceSmoke> {
        c.logger().debug("default smokers() called");
        return Observable.empty<ServiceSmoke>();
    }

    /**
     * Dispose of this microservice, including any of its resources, open handles, references, locks etc.
     * NOTICE: When overriding this method, be aware that you must still call this base method. Otherwise things will
     * not work correctly.
     */
    dispose():void {
        if (this.configuration) {
            this.configuration.dispose();
        }
    }
}
