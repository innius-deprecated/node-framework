import {assert} from "chai";
import {Service} from "./Service";

class MyService extends Service {
}

describe("Service", () => {
    describe("#Abstract service", () => {
        it("should get a name", () => {
            const s = new MyService("myname", "version", "time");
            assert.equal("myname", s.name());
            assert.equal("version", s.version());
            assert.equal("time", s.buildTime());
            setTimeout(() => s.dispose(), 10);
        });
    });
});
