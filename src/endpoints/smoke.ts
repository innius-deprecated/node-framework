import {Service} from "../service/Service";
import {hostname} from "os";
import {Observable} from "@reactivex/rxjs";

import {RequestContext} from "@toincrease/node-context";
import {whiteSmoke} from "../smokers/service_smoker";
import {Container} from "../dependencies/Container";
import {smoke_interval} from "../constants/constants";

export interface ServiceSmoke {
    name: string;
    version: string;
    buildtime: string;
    status: number;
    message: string;
    hostname: string;
    ipaddress: string;
    timestamp: number;
    dependents?: ServiceSmoke[];
}

export interface Smoker {
    (c: RequestContext, name: string): Observable<ServiceSmoke>;
}

const ok = "ok";
const statusok = 200;
export const statusInternalServerError = 500;

function message(code: number): string {
    if (code === statusok) {
        return `${new Date()}. Service is available.`;
    } else {
        return `${new Date()}. A downstream service seems to be broken.`;
    }
}

export function smoke(s: Service, ip: String, smoker: () => Observable<ServiceSmoke>):
        (c: RequestContext, di: Container) => Observable<ServiceSmoke> {
    var self: ServiceSmoke = whiteSmoke(s.name(), "not invoked yet");

    let cb = () => {
        let cur: ServiceSmoke[] = [];
        let curstate = statusok;

        smoker()
            .timeout(smoke_interval)
            .subscribe(
                (smoke) => {
                    cur.push(smoke);
                    if (!smoke || smoke.status !== statusok) {
                        curstate = statusInternalServerError;
                    }
                },
                (e) => {
                    curstate = statusInternalServerError;

                    self = <ServiceSmoke>{
                        name: s.name(),
                        version: s.version(),
                        buildtime: s.buildTime(),
                        status: statusInternalServerError,
                        message: `An error occurred: ${e}`,
                        hostname: hostname(),
                        ipaddress: ip,
                        timestamp: new Date().getTime()
                    };
                    self.dependents = cur;
                }, () => {
                    if (cur.length === 0) {
                        self = <ServiceSmoke>{
                            name: s.name(),
                            version: s.version(),
                            buildtime: s.buildTime(),
                            status: statusInternalServerError,
                            message: "No smokers defined.",
                            hostname: hostname(),
                            ipaddress: ip,
                            timestamp: new Date().getTime()
                        };
                        self.dependents = cur;
                    } else {
                        self = <ServiceSmoke>{
                            name: s.name(),
                            version: s.version(),
                            buildtime: s.buildTime(),
                            status: curstate,
                            message: message(curstate),
                            hostname: hostname(),
                            ipaddress: ip,
                            timestamp: new Date().getTime()
                        };
                        self.dependents = cur;
                    }
                });
    };

    setInterval(cb, smoke_interval);
    cb(); // Invoke straight away as well.

    return (c: RequestContext, di: Container): Observable<ServiceSmoke> => {
        return Observable.of(self);
    };
}
