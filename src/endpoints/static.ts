export class StaticRoute {
    public uri: string | RegExp;
    public path: string;
    public default_file: string;

    constructor(uri: string | RegExp, path: string) {
        this.uri = uri;
        this.path = path;
        this.default_file = undefined;
    }
}
