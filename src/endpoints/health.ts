import {Service} from "../service/Service";
import {hostname} from "os";
import {Observable} from "@reactivex/rxjs";
import {RequestContext} from "../proxy/context";
import {Container} from "../dependencies/Container";

export interface ServiceHealth {
    servicename: string;
    version: string;
    buildtime: string;
    state: string;
    hostname: string;
    timestamp: number;
}

const ok: string = "ok";

export function health(s: Service): (c: RequestContext, di: Container) => Observable<ServiceHealth> {
    return (c: RequestContext, di: Container): Observable<ServiceHealth> => {
        return Observable.of(<ServiceHealth>{
            servicename: s.name(),
            version: s.version(),
            buildtime: s.buildTime(),
            state: ok,
            hostname: hostname(),
            timestamp: new Date().getTime()
        });
    };
}
