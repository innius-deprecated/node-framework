# proxy -- reexport symbols.
This package re-exports quite a bit of functionality from underlying packages so a microservice only has to
import the ```@toincrease/node-framework``` package.

Everything in these files is exported. If something is of use only for the framework, do not export it.
One can always include the package manually.