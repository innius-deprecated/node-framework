import {Logger} from "../proxy/log";
import {Router} from "../router/Router";
import {RequestContext} from "../proxy/context";
import {jsonSink} from "../subscriber/json";
import {Container} from "../dependencies/Container";
import {Observable} from "@reactivex/rxjs";

function prefix(partial: string): string {
    return `/_admin/${partial}`;
}

export interface AdminConfig {
    router: Router;
    logger: Logger;
}

export function RegisterAdminSuite(cfg: AdminConfig): void {
    cfg.router.get(prefix("config/keys"), listKeys, jsonSink);
}

function listKeys(c:RequestContext, di:Container): Observable<string> {
    // TODO: when we upgrade node-config, call subscriptions() for a list of subscription keys from the config
    let resp: string[] = ["not", "implemented", "yet"];
    return Observable.from(resp);
}
