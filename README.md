# node-framework

An opiniated microservice framework for go services.

The framework re-exports a lot of stuff from underlying toincrease/* packages. It saves you from importing them manually.

Note: you are still required to install restify typings on every service to satisfy the typescript compiler & type hints:
```typings install restify --save --ambient```

# Wishlist
* Namespacing

# 0-day bumping
In case you need to update a core component (e.g. node-logger or something) that is transitively embedded in a lot of packages, use this order:

* node-logger
* node-http
* node-config
* node-context
* node-sdk
* node-auth-middleware
* node-framework


config: 0.0.19
sdk: 0.0.38
