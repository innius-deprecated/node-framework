var gulp = require('gulp');
var del = require('del');
var sourcemaps = require('gulp-sourcemaps');
var stream = require('stream');
var filter = require('gulp-filter');
var gitRev = require('git-rev');
var gutil = require('gulp-util');
var tslint = require('gulp-tslint');
var stylish = require('tslint-stylish');
var typescript = require('gulp-typescript');
var mocha = require('gulp-mocha');
var concat = require('gulp-concat');
var wrap = require('gulp-wrap');
var tsfmt = require('gulp-tsfmt');

var paths = {
    output: {
        main: './build',
        dev: './build/dev',
        dist: './build/dist',
        test: './build/test',
        devLib: './build/dev/lib',
        testLib: './build/test/lib'
    },
    js: 'build/**/*.js',
    ts: ['src/**/*.ts', 'typings/main/**/*.ts'],
    testDeps: []
};

/**
 * Saves a string to a file
 *
 * @param filename
 * @param string
 * @returns {*}
 */
var writeToFile = function (filename, string) {
    var src = stream.Readable({objectMode: true});
    src._read = function () {
        this.push(new gutil.File({cwd: "", base: "", path: filename, contents: new Buffer(string)}));
        this.push(null);
    };
    return src
};


var tsProject = typescript.createProject('tsconfig.json');

/**
 * Cleans all artifacts
 */
gulp.task('clean', function (callback) {
    del(paths.output.main, callback);
});

/**
 * Creates a dev build
 */
gulp.task('dev', ['js'], function () {
    gulp.watch(paths.ts, ['test', 'compile']);
});

/**
 * Creates a distributable build.
 */
gulp.task('build', ['clean', 'test', 'js']);

/**
 * Compiles typescript, gather JS libs and run tests.
 */
gulp.task('js', ['test', 'compile', 'bundle']);

gulp.task('compile', ['lint', 'version'], function () {
    var appFilter = filter(['**/*.*', '!**/*.test.js', '!**/*.d.ts']);

    var tsResult = gulp.src(paths.ts)
        .pipe(sourcemaps.init())
        .pipe(tsProject());

    tsResult.dts.pipe(gulp.dest(paths.output.dist));

    return tsResult.js
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.output.test))
        .pipe(appFilter)
        .pipe(gulp.dest(paths.output.dist));
});

gulp.task('bundle', ['compile'], function () {
  return gulp.src('build/dist/**/*.js')
    .pipe(wrap('(function(){"use strict"; <%= contents %>\n})();'))
    .pipe(concat('index.bundle.js'))
    .pipe(gulp.dest('build/dist'));
});

gulp.task('format', () => {
  gulp.src('src/**/*.ts')
    .pipe(tsfmt({
      options: {
        IndentSize: 4,
        TabSize: 4,
        NewLineCharacter: "\n",
        ConvertTabsToSpaces: true,
        InsertSpaceAfterCommaDelimiter: true,
        InsertSpaceAfterSemicolonInForStatements: true,
        InsertSpaceBeforeAndAfterBinaryOperators: true,
        InsertSpaceAfterKeywordsInControlFlowStatements: true,
        InsertSpaceAfterFunctionKeywordForAnonymousFunctions: false,
        InsertSpaceAfterOpeningAndBeforeClosingNonemptyParenthesis: false,
        PlaceOpenBraceOnNewLineForFunctions: false,
        PlaceOpenBraceOnNewLineForControlBlocks: false
      }
    }))
  .pipe(gulp.dest('./src/'));
});

/**
 * Compiles TS and runs jasmine tests
 */
gulp.task('test', ['compile'], function () {
  return gulp.src(paths.output.test + '/**/*.test.js')
    .pipe(mocha({reporter: 'dot'}));
});

gulp.task('version', function (cb) {
    gitRev.long(function (hash) {
        var json = {
            version: hash,
            datetime: new Date().toISOString()
        };
        writeToFile("version.json", JSON.stringify(json))
            .pipe(gulp.dest(paths.output.dist));
        cb();
    });
});

gulp.task('lint', function () {
    gulp.src('src/**/*.ts')
        .pipe(tslint())
        .pipe(tslint.report(stylish, {
            emitError: true,
            sort: true
        }));
});
